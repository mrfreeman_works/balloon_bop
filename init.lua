
local modname = "balloon_bop"
balloon_bop = {}
balloon_bop.T = minetest.get_translator("balloon_bop")



local round_time_default = 20


--====================================================
--====================================================
--               Minigame registration
--====================================================
--====================================================




arena_lib.register_minigame( modname , {
        name = "Balloon Bop",
        icon = "balloon_bop_hudballoon.png",

        properties = {
                starting_lives = 3,
                round_time = round_time_default,
                balloon_spawner = vector.new(0,7,0),
                spawner_range = 3.5,
                player_die_level = -1,
                arena_radius = 50,
        },

        player_properties = {
                score = 0,
        },
        temp_properties = {
                -- some minigames dont need temp properties
                num_balloons = 1,
                current_round_time = round_time_default,
                arena_lives = 1,
        },


                -- The prefix (string) is what's going to appear in most of the lines printed by your mod. Default is [Arena_lib]

        prefix = "["..modname.."] ",
        spectate = false,

        time_mode = 'incremental', -- for our sample minigame, we will use incrementing time. This will allow us to use on_time_tick if we want to.


        hotbar = {
                slots = 0,
                background_image = "blank.png", -- image not included!
                selected_image = "blank.png", -- image not included!
        },


       join_while_in_progress = true,


        in_game_physics = {
                speed = 1.5,
                jump = 1,
                gravity = 1,
                sneak = true,},


        disabled_damage_types = {"fall","punch","set_hp"},
})



--====================================================
--====================================================
--            Calling the other files
--====================================================
--====================================================


local path = minetest.get_modpath(modname)


if not minetest.get_modpath("lib_chatcmdbuilder") then
        dofile(path .. "/libraries/chatcmdbuilder.lua")
end

local manager_path = path .. "/minigame_manager/"


dofile(path .. "/leaderboard.lua")
dofile(path .. "/nodes.lua")
dofile(path .. "/items.lua")
dofile(path .. "/blocks.lua")
dofile(manager_path .. "on_load.lua")
dofile(manager_path .. "on_start.lua")
dofile(manager_path .. "on_time_tick.lua")
dofile(manager_path .. "on_celebrate.lua")





--====================================================
--====================================================
--                   Chatcommands
--====================================================
--====================================================





minetest.register_privilege( modname .."_admin", "Create and edit ".. modname .. " arenas")


local required_privs = {}

required_privs[modname .."_admin" ] = true


ChatCmdBuilder.new(modname, function(cmd)
        -- create arena
        cmd:sub("create :arena", function(name, arena_name)
            arena_lib.create_arena(name, modname, arena_name)
        end)
      
        cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
            arena_lib.create_arena(name, modname, arena_name, min_players, max_players)
        end)
      
        -- remove arena
        cmd:sub("remove :arena", function(name, arena_name)
            arena_lib.remove_arena(name, modname, arena_name)
        end)
      
        -- list of the arenas
        cmd:sub("list", function(name)
            arena_lib.print_arenas(name, modname)
        end)
      
        -- enter editor mode
        cmd:sub("edit :arena", function(sender, arena)
            arena_lib.enter_editor(sender, modname, arena)
        end)
      
        -- enable and disable arenas
        cmd:sub("enable :arena", function(name, arena)
            arena_lib.enable_arena(name, modname, arena)
        end)
      
        cmd:sub("disable :arena", function(name, arena)
            arena_lib.disable_arena(name, modname, arena)
        end)
      
      end, {
        description = [[
      
          (/help ]] .. modname .. [[)
      
          Use this to configure your arena:
          - create <arena name> [min players] [max players]
          - edit <arena name>
          - enable <arena name>
      
          Other commands:
          - remove <arena name>
          - disable <arena>
          - list (lists are created arenas)
          ]],
          privs = required_privs,
      })
      

