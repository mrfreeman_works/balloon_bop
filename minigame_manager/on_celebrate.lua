arena_lib.on_celebration("balloon_bop", function(arena, winners)
    for pl_name,stats in pairs(arena.players) do -- it is a good convention to use "pl_name" in for loops and "p_name" elsewhere
        if balloon_bop.infohuds[pl_name] then
                local player = minetest.get_player_by_name(pl_name)
                if player then
                        -- clear HUDs
                        player:hud_remove(balloon_bop.infohuds[pl_name])
                        for idx,hud_id in ipairs(balloon_bop.numhuds[pl_name]) do
                                player:hud_remove(hud_id)
                        end
                        balloon_bop.infohuds[pl_name] = nil
                        balloon_bop.numhuds[pl_name] = nil
                end
        end
        minetest.chat_send_player(pl_name,balloon_bop.T("Game Over! Your score is ").. arena.players[pl_name].score .."!")
    end
    for _,obj in pairs(minetest.get_objects_inside_radius(arena.balloon_spawner, arena.arena_radius)) do
            if not( obj:is_player()) and obj:get_luaentity().name == "balloon_bop:balloon" then
                    obj:remove()
            end
    end
end)
